"""Top-level package for Sonar Code Diff Plugin."""

__author__ = """Steve Graham"""
__email__ = "steve@ioka.tech"
__version__ = "__version__ = '0.5.0'"
