.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Sonar Code Diff Plugin, run this command in your terminal:

.. code-block:: console

    $ pip install sonar_code_diff

This is the preferred method to install Sonar Code Diff Plugin, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Sonar Code Diff Plugin can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git@gitlab.com:iokatech/public/sonar_code_diff.git

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install
